# RDF di Zotonic
Modifikasi dari mod_rdf_ginger

```
curl -L -H Accept:application/ld+json http://bsmi.dev:8000/id/102

{
	"@id": "http://bsmi.dev:8000/id/102",
	"http://purl.org/dc/terms/created": "2017-10-28T06:42:42+00:00",
	"http://purl.org/dc/terms/modified": "2017-10-28T06:42:42+00:00",
	"http://purl.org/dc/terms/issued": "2017-10-28T06:42:42+00:00",
	"http://purl.org/dc/terms/title": "Person"
}
```

```
curl -L -H Accept:application/ld+json http://bsmi.dev:8000/id/329


{
	"@id": "http://bsmi.dev:8000/id/329",
	"@type": "http://purl.org/dc/dcmitype/Text",
	"http://purl.org/dc/terms/created": "2017-10-28T06:42:44+00:00",
	"http://purl.org/dc/terms/modified": "2017-10-28T06:42:49+00:00",
	"http://purl.org/dc/terms/issued": "2012-12-14T09:12:00+00:00",
	"http://purl.org/dc/terms/description": "\n\nZotonic is well supported. Join the mailing lists or contact us for\ncommercial grade support. We have two mailing lists. One for people\nwho use Zotonic as editors, and one for people developing with or on\nZotonic.\n\nDo you need direct access to experts? Need extensions to Zotonic?\nNeed active support for a Zotonic project? We offer excellent support\nand services for all Zotonic web\nprojects. Contact us and\nexplain what you would like. We can help you.\n",
	"http://purl.org/dc/terms/abstract": "Zotonic is the content management system for people that want a fast, extensible, flexible and complete system for dynamic web sites. It is built from the ground up with rich internet applications and web publishing in mind.",
	"http://purl.org/dc/terms/title": "Welcome to Zotonic!",
	"http://purl.org/dc/terms/creator": {
		"@id": "http://bsmi.dev:8000/id/1"
	},
	"http://purl.org/dc/elements/1.1/subject": [
		{
			"@id": "http://bsmi.dev:8000/id/335"
		},
		{
			"@id": "http://bsmi.dev:8000/id/333"
		}
	],
	"http://xmlns.com/foaf/0.1/thumbnail": {
		"@id": "http://bsmi.dev:8000/image/2017/10/28/welcome.jpg%28mediaclass-foaf-thumbnail.56b969f0e8c1e384a0390286354b597588085f90%29.jpg"
	},
	"http://xmlns.com/foaf/0.1/depiction": [
		{
			"@id": "http://bsmi.dev:8000/image/2017/10/28/welcome.jpg%28%29%282F4233703F591CC7816DB8A8CA10ECFD%29.jpg"
		},
		{
			"@id": "http://bsmi.dev:8000/id/337"
		}
	]
}
```