# Dokumentasi Zotonic API
API Services sudah disediakan oleh Zotonic

## ID Resources
Mengembalikan semua id resource yang ada di database:
```
$ curl http://yoursite/api/base/everything

[1,101,102,103,104,106,107,108,109,110,111,112,113,114,115,116,117,119,120,121,122,123,124,125]
```

Jika ingin meretrive 100 ids yang pertama atau kedua bisa menggunakan parameter ```page```:
```
$ curl http://yoursite/api/base/everything?page=1
[1,101,102,103,104,106,107,108,109,110,111,112,113,114,115,116,117,119,120,121,122,123,124,125]
```

## Dump Resources
Mengembalikan 'dump' dari sebuah id resource
```
$ curl http://yoursite/api/base/export?id=102

{
	"id": 102,
	"uri": "http://bsmi.dev:8000/id/102",
	"rsc": {
		"category": "category",
		"content_group": null,
		"tz": "Asia/Jakarta",
		"date_is_all_day": false,
		"category_id": 116,
		"created": "2017-10-28T06:42:42+00:00",
		"creator_id": 1,
		"id": 102,
		"is_authoritative": true,
		"is_dependent": false,
		"is_featured": false,
		"is_protected": true,
		"is_published": true,
		"modified": "2017-10-28T06:42:42+00:00",
		"modifier_id": 1,
		"name": "person",
		"publication_start": "2017-10-28T06:42:42+00:00",
		"version": 1,
		"visible_for": 0,
		"title": {
			"trans": {
				"en": "Person",
				"nl": "Persoon"
			}
		}
	}
}

```

## Method Retrieve
Apa saja yang bisa kita lakukan dengan API Service

```
$ curl http://yoursite/api/base/meta

[
	{
		"method": "menu/menuexport",
		"module": "mod_menu",
		"service": "service_menu_menuexport",
		"title": "Export a menu resource structure as JSON.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "search/search",
		"module": "mod_search",
		"service": "service_search_search",
		"title": "Search Zotonic resources.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "admin/info",
		"module": "mod_admin",
		"service": "service_admin_info",
		"title": "Basic information about the admin module.",
		"needauth": true,
		"http": "GET,HEAD"
	},
	{
		"method": "base/meta",
		"module": "mod_base",
		"service": "service_base_meta",
		"title": "Meta-information about all API calls.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "base/persistent_get",
		"module": "mod_base",
		"service": "service_base_persistent_get",
		"title": "Retrieve a value from the Zotonic persistent record.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "base/media_upload",
		"module": "mod_base",
		"service": "service_base_media_upload",
		"title": "Upload media items.",
		"needauth": true,
		"http": "POST,PUT,DELETE"
	},
	{
		"method": "base/everything",
		"module": "mod_base",
		"service": "service_base_everything",
		"title": "Retrieve the list of all objects in the system.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "base/export",
		"module": "mod_base",
		"service": "service_base_export",
		"title": "Retrieve a full export of an object.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "base/persistent_set",
		"module": "mod_base",
		"service": "service_base_persistent_set",
		"title": "Set a value in the Zotonic persistent record.",
		"needauth": false,
		"http": "GET,HEAD"
	},
	{
		"method": "base/info",
		"module": "mod_base",
		"service": "service_base_info",
		"title": "Basic information about the system.",
		"needauth": true,
		"http": "GET,HEAD"
	}
]

```
## Informasi Website

```
http://bsmi.dev:8000/api/base/info?key=$API KEY$
```
Generate API key di Zotonic

Source code Bravyto:
https://github.com/bravyto/aisco-sem-web

## Retrieve semua resource yang termasuk ke dalam sebuah kategori

```
http://localhost:8000/api/search?cat=article&format=simple
```

```&format=simple``` untuk mendapatkan respon dalam bentuk JSON format

```
[
	{
		"id": 331,
		"title": "Zotonic&#39;s Typography",
		"category": [
			"text",
			"article"
		],
		"summary": "This article demonstrates the typographic features that Zotonic has. It shows creating ordered and unordered lists, blockquotes, and different methods of embedding media, even even showing an embedded video from Vimeo.com.",
		"preview_url": "http://bsmi.dev:8000/image/2017/11/1/welcome.jpg%28800x800%29%28upscale%29%283CE6E1DC46321342279F70E85E27D1E2%29.jpg"
	},
	{
		"id": 330,
		"title": "Want to learn more?",
		"category": [
			"text",
			"article"
		],
		"summary": "This blog website you&#39;re looking demonstrates only a small part of what you can do with a Zotonic site. For instance, did you know that sending mass-mailings is a builtin module? That it does OAuth out of the box? That Zotonic sites are SEO optimized by default?",
		"preview_url": "http://bsmi.dev:8000/image/2017/11/1/learning.jpg%28800x800%29%28upscale%29%288A8D24E19D299C24C4ED85163196B7DB%29.jpg"
	},
	{
		"id": 329,
		"title": "Welcome to Zotonic!",
		"category": [
			"text",
			"article"
		],
		"summary": "Zotonic is the content management system for people that want a fast, extensible, flexible and complete system for dynamic web sites. It is built from the ground up with rich internet applications and web publishing in mind.",
		"preview_url": "http://bsmi.dev:8000/image/2017/11/1/welcome.jpg%28800x800%29%28upscale%29%283CE6E1DC46321342279F70E85E27D1E2%29.jpg"
	}
]
```
